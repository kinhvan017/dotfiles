set nocompatible
set encoding=utf-8
set history=50

" Indenting
set autoindent
set cindent
set smartindent
"
" Tab
set expandtab
set sw=4
set ts=4

set backspace=indent,eol,start
set cursorline
set colorcolumn=80
highlight ColorColumn ctermbg=4
set nu
set ruler
set showcmd
set splitright
"set spell
syntax on
"colorscheme monakai
set hlsearch
set incsearch
set showmatch

" Python
autocmd FileType py nmap <Leader>r :!python %<CR>
autocmd FileType py nmap <Leader># ggO#!/usr/bin/env python2<CR># -*- coding: utf-8 -*-<Esc>o
autocmd FileType py nmap <Leader>m Giif __name__ == "__main__":<CR>
autocmd FileType py set omnifunc=pythoncomplete#Complete

" Shell
autocmd FileType sh nmap <Leader>r :!bash %<CR>
autocmd FileType sh nmap <Leader># ggO#!/bin/bash<Esc>o

" Do not use arrow keys
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>

" For simple status bar
function! GitBranch()
    let branch = system("git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* //'")
    if branch != ''
        return '   Git Branch: ' . substitute(branch, '\n', '', 'g')
    en
    return ''
endfunction

function! CurDir()
    return substitute(getcwd(), '/home/tungnt', "~/", "g")
endfunction

function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction
" Flake8
execute pathogen#infect()
filetype plugin indent on
"autocmd BufWritePost *.py call Flake8()
if !exists("*Flake8()")
  autocmd BufWritePost *.py call Flake8()
else
  echo 'Missing flake8 plugin'
endif


set laststatus=2
set statusline=%{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{CurDir()}%h\ \ \ Line:\ %l/%L%{GitBranch()}

map <C-n> :NERDTreeToggle<CR>

"let g:jedi#use_tabs_not_buffers = 1
let g:jedi#use_splits_not_buffers = "right"

" Highlight trailing white spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
augroup highlight_trailing_spaces
    au!
    au BufWinEnter * match ExtraWhitespace /\s\+$/
    au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    au InsertLeave * match ExtraWhitespace /\s\+$/
    au BufWinLeave * call clearmatches()
augroup END
